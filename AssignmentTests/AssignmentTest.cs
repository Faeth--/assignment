using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace AssignmentTests
{
    [TestClass]
    public class Tests
    {
        private ParserCode.Parsers p;
        Bitmap OutputBMP = new Bitmap(640, 480);
        string[] lines = new string[0];


        //<summary>
        //Tests the moveto command by checking the x and y position
        //</summary>
        [TestMethod]
        public void TestMoveTo()
        {
            
            ParserCode.Parsers p = new ParserCode.Parsers();
            CanvasCode.Canvas c = new CanvasCode.Canvas(Graphics.FromImage(OutputBMP));
            p.ParseText("moveto 100 100", 0, c, null);

            Assert.AreEqual(100, c.GetXPos(), 0.001, "X value not set");
            Assert.AreEqual(100, c.GetYPos(), 0.001, "Y value not set");
        }

        //<summary>
        //Tests the parsetext method with a syntax error
        //</summary>
        [TestMethod]
        public void TestSyntaxError()
        {
            ParserCode.Parsers p = new ParserCode.Parsers();
            CanvasCode.Canvas c = new CanvasCode.Canvas(Graphics.FromImage(OutputBMP));
            
            string s = "";

            try
            {
                p.ParseText("miveto 100 100", 0, c, null);
            }
            catch(ApplicationException e)
            {
                s = e.Message;
            }
            Assert.AreEqual("/nSyntax Error", s);
        }

        //<summary>
        //Tests the parsetext method with a parameter error
        //</summary>
        [TestMethod]
        public void TestParameterError()
        {
            ParserCode.Parsers p = new ParserCode.Parsers();
            CanvasCode.Canvas c = new CanvasCode.Canvas(Graphics.FromImage(OutputBMP));

            string s = "";

            try
            {
                p.ParseText("moveto 100", 0, c, null);
            }
            catch (IndexOutOfRangeException)
            {
                s = "Fuckin twat";
            }
            catch (FormatException)
            {
                s = "Fuckin twat";
            }
            Assert.AreEqual("Fuckin twat", s);
        }


        //<summary>
        //Tests the drawline method by checking the x and y position
        //</summary>
        [TestMethod]
        public void TestLine()
        {

            ParserCode.Parsers p = new ParserCode.Parsers();
            CanvasCode.Canvas c = new CanvasCode.Canvas(Graphics.FromImage(OutputBMP));
            p.ParseText("line 100 100", 0, c, null);

            Assert.AreEqual(100, c.GetXPos(), 0.001, "X value not set");
            Assert.AreEqual(100, c.GetYPos(), 0.001, "Y value not set");
        }

    }
}
