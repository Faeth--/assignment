﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Shapes;
using System.Windows.Media;

namespace CanvasCode
{
    /// <summary>
    /// This is the class that controls drawing images to screen
    /// </summary>
    /// <remarks>
    /// The variables within this class are the graphics context, the pen used to draw non filled drawings, the x position and y position for the cursor, a boolean for if the shape is filled or not, the brush for filled shapes, the brush for text, and the number of lines drawn to the screen
    /// </remarks>
    public class Canvas
    {
        Graphics g;
        System.Drawing.Pen pen;
        int xpos, ypos;
        bool fill = false;
        SolidBrush brush = new SolidBrush(System.Drawing.Color.Black);
        SolidBrush TextBrush = new SolidBrush(System.Drawing.Color.Black);
        int lines = 0;
        
        /// <summary>
        /// The Instansiator for the class
        /// </summary>
        /// <param name="g">The graphics context of the class</param>
        public Canvas(Graphics g)
        {
            this.g = g;
            xpos = ypos = 0;
            pen = new System.Drawing.Pen(System.Drawing.Color.Black, 1);
        }
        /// <summary>
        /// This code takes in an x and y value and draws the cursor to that position
        /// </summary>
        /// <param name="x">The x value</param>
        /// <param name="y">The y value</param>
        public void drawCursor(int x, int y)
        {
            Colour("red");
            Clear();
            xpos = x-5;
            ypos = y-5;
            DrawRect(10, 10);
        }
        
        /// <summary>
        /// This takes an image and draws it to the screen. Used for the shape variables
        /// </summary>
        /// <param name="x">The image to draw</param>
        public void DrawImage(Bitmap x)
        {
            g.DrawImageUnscaled(x, 0, 0);
        }

        /// <summary>
        /// Draw a polygon to the screen
        /// </summary>
        /// <param name="points">An array of points for the polygon</param>
        public void DrawPoly(PointF[] points)
        {
            if (fill)
            {
                g.FillPolygon(brush, points);
            }
            else
            {
                g.DrawPolygon(pen, points);
            }
        }

        /// <summary>
        /// Draw a line to the screen
        /// </summary>
        /// <param name="toX">The X value for the end of the line</param>
        /// <param name="toY">The Y value for the end of the line</param>
        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(pen, xpos, ypos, toX, toY);
            xpos = toX;
            ypos = toY;
        }


        /// <summary>
        /// Draw a circle to the screen
        /// </summary>
        /// <param name="r">The Radius of the Circle</param>
        public void DrawCircle(int r)
        {
            int halfr = (int)(r / 2);
            System.Drawing.Rectangle Rec = new System.Drawing.Rectangle((xpos - halfr), (ypos - halfr), r, r);
            if (fill)
            {
                g.FillEllipse(brush, Rec);
            }
            else
            {
                g.DrawEllipse(pen, Rec);
            }
        }

        /// <summary>
        /// Draw a rectangle to the screen
        /// </summary>
        /// <param name="width">The width of the rectangle</param>
        /// <param name="height">The height of the rectangle</param>
        public void DrawRect(int width, int height)
        {
            if (fill)
            {
                g.FillRectangle(brush, xpos, ypos, width, height);
            }
            else
            {
                g.DrawRectangle(pen, xpos, ypos, width, height);
            }
        }

        /// <summary>
        /// Change the colour of the pen and brush
        /// </summary>
        /// <param name="colour">The string relating to the colour</param>
        public void Colour(string colour)
        {
            switch(colour)
            {
                case "red":
                    pen.Color = System.Drawing.Color.Red;
                    brush.Color = System.Drawing.Color.Red;
                    break;
                case "black":
                    pen.Color = System.Drawing.Color.Black;
                    brush.Color = System.Drawing.Color.Black;
                    break;
                case "white":
                    pen.Color = System.Drawing.Color.White;
                    brush.Color = System.Drawing.Color.White;
                    break;
                case "green":
                    pen.Color = System.Drawing.Color.Green;
                    brush.Color = System.Drawing.Color.Green;
                    break;
            }
        }

        /// <summary>
        /// Draw a triangle to the screen
        /// </summary>
        /// <param name="points">An array of points for the triangle</param>
        public void Triangle (PointF[] points)
        {
            if (fill)
            {
                g.FillPolygon(brush, points);
            }
            else
            {
                g.DrawPolygon(pen, points);
            }
        }

        /// <summary>
        /// Draw text to the screen. Automatically starts at the top right and creates new lines each time it's called
        /// </summary>
        /// <param name="text">The text to be drawn</param>
        public void drawText(String text)
        {
            g.DrawString(text, (new Font("Arial", 16)), TextBrush, 0, lines);
            lines = lines + 16;
        }
        /// <summary>
        /// Clears the canvas
        /// </summary>
        public void Clear()
        {
            g.Clear(System.Drawing.SystemColors.ControlDarkDark);
        }
        /// <summary>
        /// Sets the fill value to true or false to dictate whether shapes are drawn filled or not
        /// </summary>
        /// <param name="x">A boolean value for fill</param>
        public void setFill(bool x)
        {
            fill = x;
        }
        /// <summary>
        /// The getter for fill
        /// </summary>
        /// <returns>The boolean value for fill</returns>
        public bool getFill()
        {
            return fill;
        }
        /// <summary>
        /// The setter for the cursor x position
        /// </summary>
        /// <param name="X">The X position for the cursor</param>
        public void SetXPos (int X)
        {
            xpos = X;
        }

        /// <summary>
        /// The setter for the cursor Y position
        /// </summary>
        /// <param name="Y">The Y position for the cursor</param>
        public void SetYPos(int Y)
        {
            ypos = Y;
        }

        /// <summary>
        /// The getter for the cursor X position
        /// </summary>
        /// <returns>The Cursor X position</returns>
        public int GetXPos()
        {
            return xpos;
        }
        /// <summary>
        /// The getter for the cursor Y position
        /// </summary>
        /// <returns>The Cursor Y position</returns>
        public int GetYPos()
        {
            return ypos;
        }
        /// <summary>
        /// The setter for the pen/brush colour
        /// </summary>
        /// <param name="c">The requested colour</param>
        public void SetColour(System.Drawing.Color c)
        {
            pen.Color = c;
            brush.Color = c;
        }
        /// <summary>
        /// The getter for the pen/brush collour
        /// </summary>
        /// <returns>The colour of the pen and brush</returns>
        public System.Drawing.Color GetColour()
        {
            return pen.Color;
        }
    }
}
