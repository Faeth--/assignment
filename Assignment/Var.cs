﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VarCode
{
    /// <summary>
    /// This class is used to store number variables used int he text parser
    /// </summary>
    /// <remarks>
    /// The class stores 2 variables, the value stored and the name of the variable
    /// </remarks>
    class Var
    {
        int value;
        String name;

        /// <summary>
        /// The instansiator for the class
        /// </summary>
        /// <param name="name">The name of the variable</param>
        /// <param name="value">The value of the variable</param>
        public Var (String name, int value)
        {
            this.value = value;
            this.name = "<" +name + ">";
        }

        /// <summary>
        /// The getter for the value
        /// </summary>
        /// <returns>The value of the variable</returns>
        public int getVal()
        {
            return value;
        }

        /// <summary>
        /// The setter for the variable value
        /// </summary>
        /// <param name="value">The value of the variable</param>
        public void setVal(int value)
        {
            this.value = value;
        }

        /// <summary>
        /// The getter for the variable name
        /// </summary>
        /// <returns>The variable name</returns>
        public String getName()
        {
            return name;
        }

        /// <summary>
        /// The setter for the variable name
        /// </summary>
        /// <param name="name">The variable name</param>
        public void setname(String name)
        {
            this.name = name;
        }
    }
}
