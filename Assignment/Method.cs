﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParserCode;

namespace MethodCode
{
    /// <summary>
    /// This class holds the information around the methods defined by the user
    /// </summary>
    /// <remarks>
    /// The variables in this class are for the name of the method, and then 2 arrays of strings, one for the names of the parameters and the other for the lines of the method
    /// </remarks>
    class Method
    {
        String methodName;
        String[] paramName;
        String[] Lines;
        
        /// <summary>
        /// The instansiator for the class
        /// </summary>
        /// <param name="name">An array of the names of the parameters</param>
        /// <param name="lines">The lines of the method in an array</param>
        /// <param name="methodName">The name of the method</param>
        public Method(String[] name, String[] lines, String methodName)
        {
            this.methodName = methodName;
            paramName = name;
            
            this.Lines = lines;
        }

        /// <summary>
        /// The getter for the method name
        /// </summary>
        /// <returns>The method name</returns>
        public string getName()
        {
            return methodName;
        }

        /// <summary>
        /// This method takes an array of integers for the parameters, and replaces the parameters with the values selected by the user. It then returns the string to be parsed by the parser
        /// </summary>
        /// <param name="val">An array of values to replace the parameter names with</param>
        /// <returns>A string to be parsed</returns>
        public string[] runMethod(int[] val)
        {
            if (val.Length == paramName.Length)
            {
                string[] temp = new string[Lines.Length];
                int tempPointer = 0;
                for (int i = 0; i < Lines.Length; i++)
                {
                    
                    String[] Command = Lines[i].ToLower().Split(' ');
                    for (int j = 0; j < Command.Length; j++)
                    {
                        for (int k = 0; k < val.Length; k++)
                        {
                            if (Command[j].Trim() == paramName[k])
                            {
                                Command[j] = val[k].ToString();
                            }
                            
                        }
                        if (temp[tempPointer] == null)
                        {
                            temp[tempPointer] = Command[j];
                        }
                        else
                        {
                            temp[tempPointer] = temp[tempPointer] + ' ' + Command[j];
                        }
                        
                    }
                    tempPointer++;


                }
                return temp;

            }
            else
            {
                throw new ApplicationException("/nInvalid Parameter");
            }
        }



    }
}
