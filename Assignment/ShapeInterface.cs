﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using CanvasCode;

namespace ShapeInterface

{
    /// <summary>
    /// The interface for the shape class
    /// </summary>
    /// <remarks>
    /// This could be used to define specific polygons, such as squares, trianges etc, however in this program it's used for a single shape class
    /// </remarks>
    interface Shapes
    {
        PointF[] GetPoints();

        void draw(CanvasCode.Canvas myCanvas);

    }
}
