﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanvasCode;
using ShapeInterface;

namespace ShapeCode
{
    /// <summary>
    /// This class is used to hold the shape variables and inherits the shapeinterface class
    /// </summary>
    /// <remarks>
    /// The variables in this class are an array of points, the colour of the shape, whether it's filled or not, and the name of the shape
    /// </remarks>
    class Shape : Shapes
    {
        PointF[] points;
        Color colour;
        bool fill;
        String name;

        /// <summary>
        /// This method draws the shape to the canvas
        /// </summary>
        /// <remarks>
        /// The method first stores the fill and colour values of the canvas, and then sets them back at the end
        /// </remarks>
        /// <param name="myCanvas">The canvas that the shape is to be drawn to</param>
        public void draw(Canvas myCanvas)
        {
            Color tempc = myCanvas.GetColour();
            bool tempfill = myCanvas.getFill();
            myCanvas.SetColour(colour);
            myCanvas.setFill(fill);

            myCanvas.DrawPoly(points);

            myCanvas.SetColour(tempc);
            myCanvas.setFill(tempfill);
        }

        /// <summary>
        /// The instansiator for the class
        /// </summary>
        /// <param name="points">The array of points for the shape</param>
        /// <param name="c">The colour of the shape</param>
        /// <param name="fill">The fill value of the shape</param>
        /// <param name="name">The name of the shape</param>
        public Shape(PointF[] points, Color c, bool fill, String name)
        {
            this.points = points;
            colour = c;
            this.fill = fill;
            this.name = name;
        }
        
        public String getName()
        {
            return name;
        }

        public PointF[] GetPoints()
        {
            return points;
        }
    }
}
