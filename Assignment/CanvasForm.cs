﻿using CanvasCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms;
using ParserCode;


namespace Assignment
{
    /// <summary>
    /// This is the class for the form. This is effectively the main of the program
    /// </summary>
    /// <remarks>
    /// The variables within this class are the static variables for the canvas side, the parser, the output bitmap, the file browser, and 2 canvases: one for drawing and the other for the cursor
    /// </remarks>
    public partial class  Form : System.Windows.Forms.Form
    {
        static int BMPX = 640;
        static int BMPY = 480;

        Parsers p = new Parsers();

        Bitmap OutputBMP = new Bitmap(BMPX, BMPY);

        OpenFileDialog FileBrowser;

        Canvas MyCanvas;
        Canvas CursorCanvas;

        /// <summary>
        /// The instansiator for the class, run on startup
        /// </summary>
        public Form()
        {
            InitializeComponent();
            
            MyCanvas = new Canvas(Graphics.FromImage(OutputBMP));
            
            MyCanvas.SetXPos(0);
            MyCanvas.SetYPos(0);
            DrawCursor();
            

            OutputWindow.BackgroundImage = OutputBMP; 
            
        }
        
        /// <summary>
        /// This is the event run whenever a key is pressed when the Commandline is selected. This checks if the key is enter and if so runs the commandline written
        /// </summary>
        /// <remarks>
        /// This is also what handles the exceptions thrown by the parser class
        /// </remarks>
        /// <param name="sender">This is the object that sent the event</param>
        /// <param name="e">The event that can be used to retrieve the key that was pressed</param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    Console.WriteLine("Enter Pressed");
                    String[] box = new string[1];
                    box[0] = commandLine.Text;
                    p.ParseText(box, 0, MyCanvas, CommandBox.Lines);
                    commandLine.Text = ("");
                    DrawCursor();
                    Refresh();
                }
                catch (ApplicationException n)
                {
                    if (n.Message.Equals("/nSyntax Error"))
                    {
                        MessageBox.Show("Syntax error", "Error");
                        commandLine.Text = ("");
                    }
                    else if (n.Message.Equals("/nNo endif"))
                    {
                        MessageBox.Show("No Endif", "Error");
                        commandLine.Text = ("");
                    }
                    else if (n.Message.Equals("/nInvalid Parameter"))
                    {
                        MessageBox.Show("Method Parameter Error", "Error");
                        commandLine.Text = ("");
                    }
                    else if (n.Message.Equals("/nShape not found"))
                    {
                        MessageBox.Show("Shape not found", "Error");

                    }
                    
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("Not enough parameters", "Error");
                    commandLine.Text = ("");

                }
                catch (FormatException)
                {
                    MessageBox.Show("Parameter type error", "Error");
                    commandLine.Text = ("");
                }
                catch (StackOverflowException)
                {
                    MessageBox.Show("Recursive Error", "Error");
                    commandLine.Text = ("");
                }

            }
        }

        
        /// <summary>
        /// This method draws the box around the cursor
        /// </summary>
        /// <returns>A transparent bitmap with the cursor drawn</returns>
        private Bitmap DrawCursor()
        {
            Bitmap CursorBMP = new Bitmap(BMPX, BMPY);
            
            CursorCanvas = new Canvas(Graphics.FromImage(CursorBMP));

            CursorCanvas.drawCursor(MyCanvas.GetXPos(), MyCanvas.GetYPos());
            CursorBMP.MakeTransparent();
            return CursorBMP;
            
        }
        /// <summary>
        /// This event is run every time the output window is updated
        /// </summary>
        /// <remarks>
        /// This method merges the output bitmap and the bitmap with the cursor bitmap and draws that to the screen
        /// </remarks>
        /// <param name="sender">The object that sent the event</param>
        /// <param name="e">The sender arguments</param>
        private void OutputWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(MergeBMP(OutputBMP, DrawCursor()), 0, 0);
            
        }

        /// <summary>
        /// This method takes two bitmaps and merges them into one by defining a new bitmap and drawing the other two onto them
        /// </summary>
        /// <remarks>
        /// This method is designed to work for different sizes of bitmap however that is not used in this program
        /// </remarks>
        /// <param name="Background">The Output bitmap with the users drawings on it</param>
        /// <param name="foreground">The Cursor bitmap with the cursor drawn on it</param>
        /// <returns>The merged bitmap made of the two inputs</returns>
        private Bitmap MergeBMP(Bitmap Background, Bitmap foreground) 
        {
            Bitmap result = new Bitmap(Math.Max(Background.Width, foreground.Width), Math.Max(Background.Height, foreground.Height));
            using (Graphics g = Graphics.FromImage(result))
            {
                g.DrawImage(Background, Point.Empty);
                g.DrawImage(foreground, Point.Empty);
            }
            return result;
        }

        /// <summary>
        /// This event runs whenever the save button is pressed, and opens the file browser to save the code in the commandbox as a text file
        /// </summary>
        /// <param name="sender">The button that sent the event</param>
        /// <param name="e">The event arguments</param>
        private void bSave_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileBrowser = new OpenFileDialog();
            FileBrowser.InitialDirectory = "c;\\";
            FileBrowser.Filter = "txt files (*.txt)|*.txt|All Files (*.*)|*.*";
            FileBrowser.FilterIndex = 2;
            FileBrowser.RestoreDirectory = true;
            
            if (FileBrowser.ShowDialog() == DialogResult.OK)
            {
                string path = FileBrowser.FileName;
                File.WriteAllLines(path, CommandBox.Lines);
            }
            else
            {
                
            }
        }

        /// <summary>
        /// This event runs whenever the load button is pressed, and opens the file browser to find a text file to copy into the commandbox
        /// </summary>
        /// <param name="sender">The button that sent the event</param>
        /// <param name="e">The event arguments</param>
        private void bLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileBrowser = new OpenFileDialog();
            FileBrowser.InitialDirectory = "c:\\";
            FileBrowser.Filter = "txt files (*.txt)|*.txt|All Files (*.*)|*.*";
            FileBrowser.FilterIndex = 2;
            FileBrowser.RestoreDirectory = true;
            
            if (FileBrowser.ShowDialog() == DialogResult.OK)
            {
                string path = FileBrowser.FileName;
                CommandBox.Lines = File.ReadAllLines(path);
            }
            else
            {
                MessageBox.Show("File not Found", "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
        /// <summary>
        /// This event runs whevever the colour change button is pressed, and opens up a colourdialog to select a pen colour
        /// </summary>
        /// <param name="sender">The button that sent the event</param>
        /// <param name="e">The event arguments</param>
        private void bColourChange_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
            {
                MyCanvas.SetColour(cd.Color);
            }
        }
    }
}
