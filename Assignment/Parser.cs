﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanvasCode;
using Assignment;
using System.Windows.Controls;
using System.Drawing;
using VarCode;
using MethodCode;
using ShapeCode;

namespace ParserCode
{
    /// <summary>
    /// This is the parser class that parses through the text and selects the output based upon what is inputted
    /// </summary>
    /// <remarks>
    /// This class holds a set of variables to hold information about the parsing, and also 3 lists to store users lists of variables, methods and shapes. 
    /// </remarks>
    public class Parsers
    {
        Var[] varList = new Var[50];
        int varPointer = 0;

        Method[] methodList = new Method[50];
        int methodPointer = 0;

        Shape[] shapeList = new Shape[50];
        int shapePointer = 0;

        bool pass;
        String temp;
        int tempint;
        int listint;
        bool exists;

        public Parsers()
        {

        }



        /// <summary>
        /// This is the method that parses the text.
        /// </summary>
        /// <param name="text">This is the array of strings that the method parses</param>
        /// <param name="x">The line number used for exception handling</param>
        /// <param name="MyCanvas">The canvas to draw to</param>
        /// <param name="Lines">The array in the multi line code box</param>
        /// <exception cref="ApplicationException">Thrown when the parser does not recognise the command</exception>
        /// <exception cref="IndexOutOfRangeException">Thrown when there is an incorrect number of parameters</exception>
        public void ParseText(string[] text, int x, CanvasCode.Canvas MyCanvas, String[] Lines)
        {
            for (int forlines = 0; forlines < text.Length; forlines++)
            {

                String[] Command = text[forlines].ToLower().Split(' ');
                for (int i = 0; i < Command.Length; i++)
                {
                    if (Command[i].Substring(0, 1).Equals("<"))
                    {


                        pass = false;
                        for (int k = 0; k < varPointer; k++)
                        {



                            temp = varList[k].getName();
                            if (temp.Equals(Command[i]))
                            {

                                Command[i] = varList[k].getVal().ToString();
                            }





                        }


                    }
                }



                if (Command[0].Trim().Equals("line"))
                {
                    MyCanvas.DrawLine((Int16.Parse(Command[1].Trim())), (Int16.Parse(Command[2].Trim())));

                    Console.WriteLine("Line");
                }
                else if (Command[0].Trim().Equals("rect"))
                {
                    MyCanvas.DrawRect((Int16.Parse(Command[1].Trim())), (Int16.Parse(Command[2].Trim())));

                    Console.WriteLine("Rect");
                }
                else if (Command[0].Trim().Equals("moveto"))
                {
                    MyCanvas.SetXPos(Int16.Parse(Command[1]));
                    MyCanvas.SetYPos(Int16.Parse(Command[2]));

                    Console.WriteLine("Move");
                }
                else if (Command[0].Trim().Equals("circle"))
                {
                    MyCanvas.DrawCircle(Int16.Parse(Command[1]));


                    Console.WriteLine("Circle");
                }
                else if (Command[0].Trim().Equals("run"))
                {

                    ParseText(Lines, 0, MyCanvas, Lines);

                    Console.WriteLine("Run");
                }
                else if (Command[0].Trim().Equals("clear"))
                {
                    MyCanvas.Clear();
                    Console.WriteLine("Clear");
                }
                else if (Command[0].Trim().Equals("colour"))
                {
                    MyCanvas.Colour(Command[1].Trim());
                    Console.WriteLine("Colour");
                }
                else if (Command[0].Trim().Equals("fill"))
                {
                    if (Command[1].Trim().Equals("true"))
                    {
                        MyCanvas.setFill(true);
                    }
                    else if (Command[1].Trim().Equals("false"))
                    {
                        MyCanvas.setFill(false);
                    }
                }
                else if (Command[0].Trim().Equals("triangle"))
                {
                    PointF point1 = new PointF(int.Parse(Command[1]), int.Parse(Command[2]));
                    PointF point2 = new PointF(int.Parse(Command[3]), int.Parse(Command[4]));
                    PointF point3 = new PointF(int.Parse(Command[5]), int.Parse(Command[6]));
                    PointF[] points = new PointF[3];
                    points[0] = point1;
                    points[1] = point2;
                    points[2] = point3;
                    MyCanvas.Triangle(points);
                }
                else if (Command[0].Trim().Equals("polygon"))
                {
                    PointF[] points = new PointF[(Command.Length - 1) / 2];
                    for (int i = 0; i < ((Command.Length - 1) / 2); i++)
                    {
                        PointF newPoint = new PointF(int.Parse(Command[(2 * i) + 1]), int.Parse(Command[(2 * i) + 2]));
                        points[i] = newPoint;
                    }
                    MyCanvas.DrawPoly(points);
                   
                }
                else if (Command[0].Trim().Equals("reset"))
                {
                    MyCanvas.SetXPos(0);
                    MyCanvas.SetYPos(0);
                }
                else if (Command[0].Trim().Equals("shape"))
                {
                    PointF[] Points = new PointF[(Command.Length - 2) / 2];
                    for (int i = 0; i < (Command.Length - 2)/2; i++)
                    {
                        Points[i] = new PointF(int.Parse(Command[(i * 2) + 2]), int.Parse(Command[(i * 2) + 3]));
                    }

                    shapeList[shapePointer] = new Shape(Points, MyCanvas.GetColour(), MyCanvas.getFill(), Command[1]);
                    shapePointer++;
                }
                else if (Command[0].Trim().Equals("drawshape"))
                {
                    bool pass = false;
                    for (int i = 0; i < shapePointer; i++)
                    {
                        if (Command[1].Trim().Equals(shapeList[i].getName()))
                        {
                            shapeList[i].draw(MyCanvas);
                            pass = true;
                        }
                    }
                    if (!pass)
                    {
                        throw new ApplicationException("/nShape not found");
                    }
                }
                else if (Command[1].Trim().Equals("="))
                {
                    if (Command.Length > 3)
                    {
                        exists = false;

                        for (int l = 0; l < varPointer; l++)
                        {

                            if (varList[l].getName() == "<" + Command[0].Trim() + ">")
                            {
                                listint = l;
                                exists = true;
                            }

                            if (varList[l].getName() == "<" + Command[2].Trim() + ">")
                            {
                                Command[2] = varList[l].getVal().ToString();
                            }

                            if (varList[l].getName() == "<" + Command[4].Trim() + ">")
                            {
                                Command[4] = varList[l].getVal().ToString();
                            }


                        }

                        if (exists)
                        {
                            if (Command[3] == "+")
                            {
                                varList[listint].setVal(int.Parse(Command[2]) + int.Parse(Command[4]));
                            }
                            else if (Command[3] == "-")
                            {
                                varList[listint].setVal(int.Parse(Command[2]) - int.Parse(Command[4]));
                            }
                            else if (Command[3] == "*")
                            {
                                varList[listint].setVal(int.Parse(Command[2]) * int.Parse(Command[4]));
                            }
                            else if (Command[3] == "/")
                            {
                                varList[listint].setVal(int.Parse(Command[2]) / int.Parse(Command[4]));
                            }
                        }
                        else
                        {
                            if (Command[3] == "+")
                            {
                                varList[varPointer] = new Var(Command[0].Trim(), (int.Parse(Command[2]) + int.Parse(Command[4])));
                            }
                            else if (Command[3] == "-")
                            {
                                varList[varPointer] = new Var(Command[0].Trim(), (int.Parse(Command[2]) - int.Parse(Command[4])));
                            }
                            else if (Command[3] == "*")
                            {
                                varList[varPointer] = new Var(Command[0].Trim(), (int.Parse(Command[2]) * int.Parse(Command[4])));
                            }
                            else if (Command[3] == "/")
                            {
                                varList[varPointer] = new Var(Command[0].Trim(), (int.Parse(Command[2]) / int.Parse(Command[4])));
                            }
                            varPointer++;
                        }


                    }
                    else
                    {
                        varList[varPointer] = new Var(Command[0].Trim(), int.Parse(Command[2]));
                        varPointer++;
                    }
                }
                else if (Command[0].Trim().Equals("if"))
                {
                    int start = forlines + 1;
                    string[] subarray;
                    bool pass = false;

                    do
                    {
                        if (text[forlines].Equals("endif"))
                        {

                            pass = true;


                        }
                        else
                        {

                            forlines++;
                        }
                        
                    } while (!(pass) && !(forlines >= text.Length));


                    if (pass)
                    {

                        int end = forlines;
                        subarray = new string[end - start];

                        int arraypointer = 0;
                        for (int a = start; a < end; a++)
                        {
                            subarray[arraypointer] = Lines[a];
                            arraypointer++;
                        }

                        int n1 = int.Parse(Command[1]);
                        int n2 = int.Parse(Command[3]);
                        if (Command[2].Equals("=="))
                        {
                            if (n1 == n2)
                            {
                                ParseText(subarray, 0, MyCanvas, Lines);
                            }
                        }
                        else if (Command[2].Equals(">"))
                        {
                            if (n1 > n2)
                            {
                                ParseText(subarray, 0, MyCanvas, Lines);
                            }
                        }
                        else if (Command[2].Equals("<"))
                        {
                            if (n1 < n2)
                            {
                                ParseText(subarray, 0, MyCanvas, Lines);
                            }
                        }
                        else if (Command[2].Equals(">="))
                        {
                            if (n1 >= n2)
                            {
                                ParseText(subarray, 0, MyCanvas, Lines);
                            }
                        }
                        else if (Command[2].Equals("<="))
                        {
                            if (n1 >= n2)
                            {
                                ParseText(subarray, 0, MyCanvas, Lines);
                            }
                        }

                       
                    }
                    else
                    {
                        throw new ApplicationException("/nNo endif");
                    }

                }
                else if (Command[0].Trim().Equals("loop"))
                {
                    int loops = int.Parse(Command[1]);
                    int start = forlines + 1;
                    string[] subarray;
                    bool pass = false;

                    do
                    {
                        if (text[forlines].Equals("endloop"))
                        {

                            pass = true;


                        }
                        else
                        {

                            forlines++;
                        }

                    } while (!(pass) && !(forlines >= text.Length));

                    if (pass)
                    {
                        int end = forlines;
                        subarray = new string[end - start];

                        int arraypointer = 0;
                        for (int a = start; a < end; a++)
                        {
                            subarray[arraypointer] = Lines[a];
                            arraypointer++;
                        }

                        for (int m = 0; m < loops; m++)
                        {
                            ParseText(subarray, 0, MyCanvas, Lines);
                        }
                    }
                }
                else if (Command[0].Trim().Equals("method"))
                {
                    int start = forlines + 1;
                    string[] subarray;
                    bool pass = false;
                    string[] param = new string[Command.Length - 2];
                    int paramPointer = 0;
                    for (int i = 2; i < Command.Length; i++)
                    {
                        param[paramPointer] = Command[i];
                        paramPointer++;
                    }

                    do
                    {
                        if (text[forlines].Equals("endmethod"))
                        {

                            pass = true;



                        }
                        else
                        {

                            forlines++;
                        }
                    } while (!(pass) && !(forlines >= text.Length));


                    if (pass)
                    {

                        int end = forlines;
                        subarray = new string[end - start];

                        int arraypointer = 0;
                        for (int a = start; a < end; a++)
                        {
                            subarray[arraypointer] = Lines[a];
                            arraypointer++;
                        }

                        methodList[methodPointer] = new Method(param, subarray, Command[1]);

                    }
                }
                else
                {
                    if (!(methodList == null))
                    {
                        bool pass = false;
                        int tempint = 51;
                        int[] param = new int[Command.Length - 1];
                        int paramPointer = 0;
                        for (int i = 1; i < Command.Length; i++)
                        {
                            param[paramPointer] = int.Parse(Command[i]);
                            paramPointer++;
                        }

                        for (int i = 0; i <= methodPointer; i++)
                        {
                            if (Command[0] == methodList[i].getName())
                            {
                                pass = true;
                                tempint = i;
                            }
                        }

                        if (pass)
                        {
                            ParseText(methodList[tempint].runMethod(param), x, MyCanvas, Lines);
                        }
                        else
                        {
                            throw new ApplicationException("/nSyntax Error");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("/nSyntax Error");
                    }

                }

            }

        }
    }
}

